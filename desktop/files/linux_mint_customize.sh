#!/bin/bash

if [ $XDG_CURRENT_DESKTOP == "X-Cinnamon" ]
then

  GSETTINGS_FILE_MANAGER=nemo
  GSETTINGS_LOCK=cinnamon.desktop
  GSETTINGS_LOCK_DELAY="uint32 1800"
  GSETTINGS_KEYBOARD_GROUP=gnome.libgnomekbd.desktop
  GSETTINGS_KEYBOARD_SWITCH=gnome.libgnomekbd.keyboard

  echo "Отключаем историю недавно открытых файлов..."
  gsettings set org.cinnamon.desktop.privacy remember-recent-files false

elif [ $XDG_CURRENT_DESKTOP == "MATE" ]
then

  GSETTINGS_FILE_MANAGER=mate.caja
  GSETTINGS_LOCK=mate
  GSETTINGS_LOCK_DELAY=30
  GSETTINGS_KEYBOARD_GROUP=mate.peripherals-keyboard-xkb.general
  GSETTINGS_KEYBOARD_SWITCH=mate.peripherals-keyboard-xkb.kbd

fi

echo "Настраиваем файловый менеджер..."
gsettings set org.$GSETTINGS_FILE_MANAGER.desktop computer-icon-visible true
gsettings set org.$GSETTINGS_FILE_MANAGER.desktop home-icon-visible true
gsettings set org.$GSETTINGS_FILE_MANAGER.desktop trash-icon-visible true
gsettings set org.$GSETTINGS_FILE_MANAGER.preferences enable-delete false
gsettings set org.$GSETTINGS_FILE_MANAGER.preferences show-image-thumbnails 'local-only'

echo "Настраиваем экранную заставку..."
gsettings set org.$GSETTINGS_LOCK.screensaver lock-enabled false
gsettings set org.$GSETTINGS_LOCK.session idle-delay "$GSETTINGS_LOCK_DELAY"

echo "Настраиваем клавиатуру..."
gsettings set org.$GSETTINGS_KEYBOARD_GROUP group-per-window false
gsettings set org.$GSETTINGS_KEYBOARD_SWITCH options "['terminate\tterminate:ctrl_alt_bksp', 'grp\tgrp:alt_shift_toggle']"

echo "Отключаем автозагрузку экрана приветствия..."
mkdir -p ~/.linuxmint/mintwelcome
touch ~/.linuxmint/mintwelcome/norun.flag

echo "Копируем шаблоны документов..."
curl -s https://gitlab.com/vednil/kb/-/raw/main/desktop/files/libreoffice_calc.ods -o ~/Шаблоны/LibreOffice\ Calc.ods
curl -s https://gitlab.com/vednil/kb/-/raw/main/desktop/files/libreoffice_writer.odt -o ~/Шаблоны/LibreOffice\ Writer.odt
