# Установка Linux Mint

[[_TOC_]]



## Установка системы

1. Скачиваем образ `Linux Mint 22` (редакцию `Cinnamon` или `MATE`) с сайта https://linuxmint.com.
2. Записываем образ на USB-накопитель, например, с помощью [Rufus](https://rufus.ie) или [Ventoy](https://www.ventoy.net/).
3. Запускаем компьютер с подготовленного USB-накопителя.
   - _Установка операционной системы в режиме UEFI производится с отключенным Secure Boot._
4. Подключаем компьютер к интернету.
5. Устанавливаем операционную систему с помощью значка `Install Linux Mint` на рабочем столе.
   - Выбираем русский язык.
   - Оставляем русскую раскладку.
   - Устанавливаем галочку `Установка мультимедиа кодеков`.
   - Выбираем другой вариант установки.
   - Выполняем разбивку диска:
      - создаем новую таблицу разделов;
      - если компьютер запущен в режиме UEFI, то создаем `системный раздел EFI` объемом 128 МБ;
      - создаем раздел `/` объемом 122 880 МБ, то есть 120 ГБ;
      - создаем раздел `/home` из оставшегося свободного места на диске;
      - запускаем установку и подтверждаем изменения диска.
   - Выбираем часовой пояс `Moscow`.
   - Указываем имя пользователя и пароль, а также имя компьютера.
6. Перезагружаем компьютер после завершения установки.



## Настройка системы

1. Настраиваем интерфейс:
```
curl -s https://gitlab.com/vednil/kb/-/raw/main/desktop/files/linux_mint_customize.sh | bash
```
2. Меняем зеркало пакетов:
```
sudo sed -i "s/packages.linuxmint.com/fastly.linuxmint.io/g" /etc/apt/sources.list.d/official-package-repositories.list 
```
3. Обновляем систему:
```
sudo apt-get update && \
sudo apt-get -y dist-upgrade
```
4. Включаем автоматическое обновление системы и удаление старых ядр и зависимостей:
```
sudo mintupdate-automation upgrade enable && \
sudo mintupdate-automation autoremove enable
```
5. Настраиваем использование swap:
```
echo "vm.swappiness = 5" | sudo tee /etc/sysctl.d/99-swappiness.conf && \
sudo sysctl -p /etc/sysctl.d/99-swappiness.conf
```
6. Устанавливаем numlockx и включаем numlock в окне входа в систему:
```
sudo apt-get install -y numlockx
```
```
cat <<EOF | sudo tee /etc/lightdm/slick-greeter.conf
[Greeter]
activate-numlock=true
EOF
```



## Дополнительные действия

_Инструкции данного раздела необязательны и выполняются для включения поддержки некоторого оборудования._

- Устанавливаем драйвер видеокарт Nvidia:
```
VERSION=550
```
```
sudo apt-get install -y nvidia-driver-$VERSION
```
```
cat <<EOF | sudo tee /etc/modules-load.d/nvidia.conf
nvidia
nvidia-drm
nvidia-modeset
EOF
```
- Устанавливаем драйвер сетевых карт Broadcom для включения поддержки Wi-Fi:
```
sudo apt-get install -y bcmwl-kernel-source
```
- Обновляем ядро:
```
sudo apt-get install -y linux-generic-hwe-24.04
```
